Bat file to simplify using bundletool. Provides with bundletool.jar v0.13.0

Using: bundletool.bat path_to_aab path_to_keystore keystore_pass keystore_alias alias_pass unzip_needed(opt) install_needed(opt, empty - not needed, 1 - armeabi_v7a, 2 - arm64_v8a)

Example: bundletool.bat path_to\Build.aab path_to\appname.keystore pass_1 alias_name pass_2 true 1
Will convert .aab file to .apks archive, extract it and will install it to device using adb.