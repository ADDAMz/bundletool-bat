:: simple script for bundletool tool (converting .aab to .apk)
:: using: bundletool.bat path_to_aab path_to_keystore keystore_pass keystore_alias alias_pass unzip_needed(opt) install_needed(opt, empty - not needed, 1 - armeabi_v7a, 2 - arm64_v8a)

:: @echo on
set path_to_aab=%1
set path_to_keystore=%2
set keystore_pass=%3
set keystore_alias=%4
set alias_pass=%5
set unzip_needed=%6
set install_needed=%7

set mypath=%~dp0

del %path_to_aab:.aab=.apks%

java -jar %mypath%bundletool-all-0.13.0.jar build-apks --bundle=%path_to_aab% --output=%path_to_aab:.aab=.apks% --ks=%path_to_keystore% --ks-pass=pass:%keystore_pass% --ks-key-alias=%keystore_alias% --key-pass=pass:%alias_pass%

if "%unzip_needed%"=="true" (
	mkdir %path_to_aab:.aab=%
	%mypath%unzip.exe -o %path_to_aab:.aab=.apks% -d %path_to_aab:.aab=%
	
	if "%install_needed%"=="1" (
		adb install -r %path_to_aab:.aab=%/standalones/standalone-armeabi_v7a.apk
	) else if "%install_needed%"=="2" (
		adb install -r %path_to_aab:.aab=%/standalones/standalone-arm64_v8a.apk
	)
)

pause